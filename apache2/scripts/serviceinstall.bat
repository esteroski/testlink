@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:/Users/evarg/OneDrive/Documents/TestLink/apache2\bin\httpd.exe" -k install -n "testlinkApache" -f "C:/Users/evarg/OneDrive/Documents/TestLink/apache2\conf\httpd.conf"

net start testlinkApache >NUL
goto end

:remove
rem -- STOP SERVICE BEFORE REMOVING

net stop testlinkApache >NUL
"C:/Users/evarg/OneDrive/Documents/TestLink/apache2\bin\httpd.exe" -k uninstall -n "testlinkApache"

:end
exit
