@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\kibana\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\kibana\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\serviceinstall.bat" INSTALL)
rem RUBY_APPLICATION_INSTALL
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\nginx\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\nginx\scripts\serviceinstall.bat" INSTALL)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\php\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\php\scripts\serviceinstall.bat" INSTALL)
goto end

:remove

if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\serviceinstall.bat")
rem RUBY_APPLICATION_REMOVE
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\kibana\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\kibana\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\php\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\php\scripts\serviceinstall.bat")
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\nginx\scripts\serviceinstall.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\nginx\scripts\serviceinstall.bat")
:end
