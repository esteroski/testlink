@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop

if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\hypersonic\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\server\hsql-sample-database\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\ingres\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\ingres\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jetty\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jetty\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\servicerun.bat" START)
rem RUBY_APPLICATION_START
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\servicerun.bat" START)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\servicerun.bat" START)
goto end

:stop
echo "Stopping services ..."
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\third_application\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\lucene\scripts\servicerun.bat" STOP)
rem RUBY_APPLICATION_STOP
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\subversion\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jetty\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jetty\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\hypersonic\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\server\hsql-sample-database\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\jboss\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\wildfly\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\resin\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\activemq\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\openoffice\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache2\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\servicerun.bat" (start "" /MIN /WAIT "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\apache-tomcat\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash-forwarder\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\logstash\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\elasticsearch\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\ingres\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\ingres\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mysql\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\mongodb\scripts\servicerun.bat" STOP)
if exist "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\servicerun.bat" (start "" /MIN "C:\Users\evarg\OneDrive\DOCUME~1\TestLink\postgresql\scripts\servicerun.bat" STOP)

:end
