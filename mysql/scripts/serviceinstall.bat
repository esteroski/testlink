@echo off
rem -- Check if argument is INSTALL or REMOVE

if not ""%1"" == ""INSTALL"" goto remove

"C:\Users\evarg\OneDrive\Documents\TestLink/mysql\bin\mysqld.exe" --install "testlinkMySQL" --defaults-file="C:\Users\evarg\OneDrive\Documents\TestLink/mysql\my.ini"

net start "testlinkMySQL" >NUL
goto end

:remove
rem -- STOP SERVICES BEFORE REMOVING

net stop "testlinkMySQL" >NUL
"C:\Users\evarg\OneDrive\Documents\TestLink/mysql\bin\mysqld.exe" --remove "testlinkMySQL"

:end
exit
